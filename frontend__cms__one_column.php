<?
class wp_papp__frontend__cms__one_column extends wp_papp__frontend__cms__one_column__parent
{
	function load($d = null)
	{
		parent::{__function__}();
		$this->C->cms()->get_page();
	}
	
	function show($d = null)
	{
		$this->D['TEMPLATE']['FILE'][] = basename(__file__,'.php').".tpl";
		parent::{__function__}();
		$this->C->template()->display();
	}
}