{block name="meta" append}
	<meta name='title' content='{$D.MODUL.D['wp_cms'].PAGE.D[$D.MODUL.D['wp_cms'].PAGE.W.ID].LANGUAGE.D['DE'].ATTRIBUTE.D['TITLE'].VALUE}'/>
{/block}
{block name="content"}
<div class="container bs-docs-container">
	<div class="row">
		{if $D.MODUL.D['wp_cms'].MENU.ACTIVE_MENU_ID && $D.MODUL.D['wp_cms'].MENU.PARENT.D[$D.MODUL.D['wp_cms'].MENU.ACTIVE_MENU_ID]|COUNT > 0}
		<div class="col-md-3" role="complementary">
			<nav class="bs-docs-sidebar hidden-print hidden-sm hidden-xs affix">
				<ul class="nav bs-docs-sidenav">
					{foreach from=$D.MODUL.D['wp_cms'].MENU.PARENT.D[$D.MODUL.D['wp_cms'].MENU.ACTIVE_MENU_ID].CHILD.D name=STRUCT item=STRUCT key=kSTRUCT}
						{if $D.MODUL.D['wp_cms'].MENU.PARENT.D[$kSTRUCT].CHILD.D|COUNT > 0}
							<li class=""> <a href="{$D.MODUL.D['wp_cms'].MENU.D[$kSTRUCT].LANGUAGE.D['DE'].URL}">{$D.MODUL.D['wp_cms'].MENU.D[$kSTRUCT].LANGUAGE.D['DE'].TITLE}</a>
						{/if}
					{/foreach}
				</ul>
			</nav>
		</div>
		<div class="col-md-9" role="complementary">
		{else}
		<div role="complementary">
		{/if}
			<div class="page-header">
				<h1>{$D.MODUL.D['wp_cms'].PAGE.D[$D.MODUL.D['wp_cms'].PAGE.W.ID].LANGUAGE.D['DE'].ATTRIBUTE.D['TITLE'].VALUE}</h1>
			</div>
			{$D.MODUL.D['wp_cms'].PAGE.D[$D.MODUL.D['wp_cms'].PAGE.W.ID].LANGUAGE.D['DE'].ATTRIBUTE.D['TEXT'].VALUE}
		</div>
	</div>
</div>
{/block}
