<!DOCTYPE html>
<html>
{block name="<html>"}
	<head>
	{block name="<head>"}
		{block name="meta"}
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		{/block}
		{block name="stylesheet"}
			<link rel="preload" as="style" href="{minify input=array('tmp/system/library/bootstrap/css/bootstrap.min.css','tmp/system/library/bootstrap/css/font-awesome.min.css') output='tmp/css.css' use_true_path=true age=30}" onload="this.rel='stylesheet'">
			<noscript><link rel="stylesheet" href="{minify input=array('tmp/system/library/bootstrap/css/bootstrap.min.css','tmp/system/library/bootstrap/css/font-awesome.min.css') output='tmp/css.css' use_true_path=true age=30}" media="all"></noscript>
			<script>
			/*! loadCSS. [c]2017 Filament Group, Inc. MIT License */
			!function(a){ "use strict";var b=function(b,c,d){ function e(a){ return h.body?a():void setTimeout(function(){ e(a)})}function f(){ i.addEventListener&&i.removeEventListener("load",f),i.media=d||"all"}var g,h=a.document,i=h.createElement("link");if(c)g=c;else{ var j=(h.body||h.getElementsByTagName("head")[0]).childNodes;g=j[j.length-1]}var k=h.styleSheets;i.rel="stylesheet",i.href=b,i.media="only x",e(function(){ g.parentNode.insertBefore(i,c?g:g.nextSibling)});var l=function(a){ for(var b=i.href,c=k.length;c--;)if(k[c].href===b)return a();setTimeout(function(){ l(a)})};return i.addEventListener&&i.addEventListener("load",f),i.onloadcssdefined=l,l(f),i};"undefined"!=typeof exports?exports.loadCSS=b:a.loadCSS=b}("undefined"!=typeof global?global:this);
			/*! loadCSS rel=preload polyfill. [c]2017 Filament Group, Inc. MIT License */
			!function(a){ if(a.loadCSS){ var b=loadCSS.relpreload={ };if(b.support=function(){ try{ return a.document.createElement("link").relList.supports("preload")}catch(b){ return!1}},b.poly=function(){ for(var b=a.document.getElementsByTagName("link"),c=0;c<b.length;c++){ var d=b[c];"preload"===d.rel&&"style"===d.getAttribute("as")&&(a.loadCSS(d.href,d,d.getAttribute("media")),d.rel=null)}},!b.support()){ b.poly();var c=a.setInterval(b.poly,300);a.addEventListener&&a.addEventListener("load",function(){ b.poly(),a.clearInterval(c)}),a.attachEvent&&a.attachEvent("onload",function(){ a.clearInterval(c)})}}}(this);
			</script>
		{/block}
		{block name="<script>"}
			<script src="{minify input=array('tmp/system/library/jquery/jquery-2.2.4.min.js','tmp/system/library/bootstrap/js/bootstrap.min.js') output='tmp/js.js' use_true_path=true async=true age=30}" crossorigin="anonymous" async></script>
			{*<script src="library/jquery/jquery-2.2.4.min.js" crossorigin="anonymous" ></script>*}
			{*<script src="library/jquery/jquery-3.2.1.min.js" crossorigin="anonymous"></script>*}
			{*<script src="library/bootstrap/js/bootstrap.min.js" crossorigin="anonymous" ></script>*}
		{/block}
	{/block}
	</head>
	<body>
	{block name="<body>"}
		<header>
			<nav class="navbar navbar-default" style="position:;width:100%;z-index:1000;">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand" href="/"><i class="fa fa-home"></i></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
					{foreach from=$D.MODUL.D['wp_cms'].MENU.PARENT.D['TOP_MENU'].CHILD.D name=STRUCT item=STRUCT key=kSTRUCT}
						{if $D.MODUL.D['wp_cms'].MENU.PARENT.D[$kSTRUCT].CHILD.D|COUNT > 0}
						<li class="dropdown">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{$D.MODUL.D['wp_cms'].MENU.D[$kSTRUCT].LANGUAGE.D['DE'].TITLE}<span class="caret"></span></a>
						  <ul class="dropdown-menu">
							{foreach from=$D.MODUL.D['wp_cms'].MENU.PARENT.D[$kSTRUCT].CHILD.D name=STRUCT2 item=STRUCT2 key=kSTRUCT2}
								<li><a href="/{$D.MODUL.D['wp_cms'].MENU.D[$kSTRUCT2].LANGUAGE.D['DE'].URL}">{$D.MODUL.D['wp_cms'].MENU.D[$kSTRUCT2].LANGUAGE.D['DE'].TITLE}</a></li>
							{/foreach}
						  </ul>
						</li>
						{else}
						<li><a href="/{$D.MODUL.D['wp_cms'].MENU.D[$kSTRUCT].LANGUAGE.D['DE'].URL}">{$D.MODUL.D['wp_cms'].MENU.D[$kSTRUCT].LANGUAGE.D['DE'].TITLE} <span class="sr-only">(current)</span></a></li>
						{/if}
					{/foreach}
					</ul>
				  
				  <ul class="nav navbar-nav navbar-right">
					{if $D.LANGUAGE.D}
					<li class="dropdown">
					  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-language" aria-hidden="true"></i> <span class="caret"></span></a>
					  <ul class="dropdown-menu">
						{foreach from=$D.LANGUAGE.D item=LG key=kLG}
						<li class="{if $D.LANGUAGE.ACTIVE_LANGUAGE == $kLG}active{/if}"><a href="{$D.SEO_URL}?D[LANGUAGE][ACTIVE_LANGUAGE]={$kLG}">{$LG.TITLE}</a></li>
						{/foreach}
					  </ul>
					</li>
					{/if}
				  </ul>
				  
				  <form class="navbar-form navbar-right">
					<div class="input-group">
					  <input type="text" class="form-control" placeholder="Search">
					  <span class="input-group-btn">
					  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
					  </span>
					</div>
				  </form>
				  
				</div>
			  </div>
			</nav>
		</header>
		<div class="container-fluid">
		{block name="content"}{/block}
		</div>
	{/block}
	</body>
{/block}
<html>